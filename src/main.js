let data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];

let html = '';
/*
//tri par nom
data.sort(function (a, b) {
    return a.name.localeCompare(b.name);
});
*/

/*
//tri par petit prix
data.sort(function (a, b) {
    return a.price_small - b.price_small;
});


//tri par petit prix, puis grand prix
data.sort(function(a,b){
    if((a.price_small - b.price_small) == 0)
        return a.price_large - b.price_large;
    else 
        return a.price_small - b.price_small;
});

//affichage des pizza dont la base est tomate
data = data.filter(word => word.base == 'tomate');

//affichage des pizza dont le prix petit format < 6
data = data.filter(prix => prix.price_small < 6)
*/
//affichage des pizza dont le nom contient deux fois la lettre "i"
data = data.filter(nom => nom.name.)

data.forEach(function(value){
    let url = value.image;
    html = html + '<article class="pizzaThumbnail"> <a href="'+url+'"> <img src="'+url+'"/> <section> <h4>'+ value.name +'</h4><ul><li>Prix petit format : ' + value.price_small + '€</li><li>Prix grand format : ' + value.price_large + '€</li></ul></section> </a> </article>\n\n';
});

document.querySelector('.pageContent').innerHTML = html;
console.log(html);